﻿// memeto.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include <iostream>
#include <stack>
#include <string.h>

using namespace std;

class Memento
{
public:
	Memento(string _state) { state = _state; }

public:
	string getState() const { return state; }

private:
	string state;
};

class Caretaker
{
public:
	void pushMemeto(Memento* m) { mStack.push(m); }
	Memento* popMemeto() { Memento* m = mStack.top(); mStack.pop(); return m; }

private:
	stack<Memento*> mStack;
};

class Originator
{
public:
	void setState(string _state)
	{
		state = _state;
		cout << "Originator: Setting state to " << state.c_str() << endl;
	}

public:
	void setMemeto(Memento *m)
	{
		if (m)
		{
			state = m->getState();
			delete m;
			cout << "Originator: State after restoring from Memento " << state.c_str() << endl;
		}
	}

	Memento* createMemento()
	{
		cout << "Originator: Create to Memento " << state.c_str() << endl;
		return new Memento(state);
	}

private:
	string state;
};


int main()
{
	Caretaker mCaretaker;
	Originator* originator = new Originator();

	originator->setState("state1");
	mCaretaker.pushMemeto(originator->createMemento());

	originator->setState("state2");
	mCaretaker.pushMemeto(originator->createMemento());

	originator->setMemeto(mCaretaker.popMemeto());
	originator->setMemeto(mCaretaker.popMemeto());

	delete originator;
}
