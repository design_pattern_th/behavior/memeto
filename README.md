README.md
===

memeto : 오브젝트의 '과거' 정보를 유지하고 싶을때 사용한다. (장기/바둑의 복기 같은 것)

![이미지](/image/Memeto_Pattern_image1.jpg)<br>

```c

void main() {
    // Caretaker : 오브젝트의 '과거' 정보를 관리 하는 클래스
    Caretaker mCaretaker;

    // Originator : 알고리즘 사용 오브젝트
    Originator* originator = new Originator();

    // 정보를 state1으로 설정하라
    originator->setState("state1");

    // 설정 정보를 관리
    mCaretaker.pushMemento(originator->createMemento());

    // 정보를 state2으로 설정하라
    originator->setState("state2");

    // 설정 정보를 관리
    mCaretaker.pushMemento(originator->createMemento());

    // 이전정보를 불러오고 싶다.
    originator->setMemeto(mCaretaker.popMemento());
    originator->setMemeto(mCaretaker.popMemento());

    delete originator;
}

```